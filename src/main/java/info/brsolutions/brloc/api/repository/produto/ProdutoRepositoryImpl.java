package info.brsolutions.brloc.api.repository.produto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import info.brsolutions.brloc.api.model.Produto;
import info.brsolutions.brloc.api.model.Produto_;
import info.brsolutions.brloc.api.repository.filter.ProdutoFilter;

public class ProdutoRepositoryImpl implements ProdutoRepositoryQuery {
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Produto> filtra(ProdutoFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		
		CriteriaQuery<Produto> criteria = builder.createQuery(Produto.class);
		
		Root<Produto> root = criteria.from(Produto.class);
		
		Predicate[] predicates = this.criaRestricoes(filtro, builder, root);
		
		criteria.where(predicates);
		
		TypedQuery<Produto> query = this.manager.createQuery(criteria);
		
		this.adicionaRestricoesPaginacao(query, pageable);
		
		return new PageImpl<Produto>(query.getResultList(), pageable, this.total(filtro));
	}

	private Predicate[] criaRestricoes(ProdutoFilter filtro, CriteriaBuilder builder, Root<Produto> root) {

		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if (!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get(Produto_.NOME)), 
					"%" + filtro.getNome().toLowerCase() + "%"));
		}
		
		if (!StringUtils.isEmpty(filtro.getDescricao())) {
			predicates.add(builder.like(builder.lower(root.get(Produto_.DESCRICAO)), 
					"%" + filtro.getDescricao().toLowerCase() + "%"));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	

	private void adicionaRestricoesPaginacao(TypedQuery<Produto> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistro = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistro);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private long total(ProdutoFilter filtro) {

		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Produto> root = criteria.from(Produto.class);
		
		Predicate[] predicates = this.criaRestricoes(filtro, builder, root);
		
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		
		return this.manager.createQuery(criteria).getSingleResult();
	}


}
