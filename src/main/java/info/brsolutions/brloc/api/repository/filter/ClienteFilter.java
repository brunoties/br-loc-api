package info.brsolutions.brloc.api.repository.filter;

import info.brsolutions.brloc.api.model.TipoCliente;

public class ClienteFilter {
	
	private String nome;
	private TipoCliente tipo;
	private String documentoFederal;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public TipoCliente getTipo() {
		return tipo;
	}
	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo;
	}
	public String getDocumentoFederal() {
		return documentoFederal;
	}
	public void setDocumentoFederal(String documentoFederal) {
		this.documentoFederal = documentoFederal;
	}

}
