package info.brsolutions.brloc.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import info.brsolutions.brloc.api.model.Equipamento;
import info.brsolutions.brloc.api.repository.equipamento.EquipamentoRepositoryQuery;

public interface EquipamentoRepository extends JpaRepository<Equipamento, Long>, EquipamentoRepositoryQuery {

}
