package info.brsolutions.brloc.api.repository.produto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import info.brsolutions.brloc.api.model.Produto;
import info.brsolutions.brloc.api.repository.filter.ProdutoFilter;

public interface ProdutoRepositoryQuery {
	
	public Page<Produto> filtra(ProdutoFilter filtro, Pageable pageable);

}
