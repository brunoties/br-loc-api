package info.brsolutions.brloc.api.repository.categoriaequipamentos;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import info.brsolutions.brloc.api.model.CategoriaEquipamentos;
import info.brsolutions.brloc.api.repository.filter.CategoriaEquipamentosFilter;

public interface CategoriaEquipamentosRepositoryQuery {
	public Page<CategoriaEquipamentos> filtra (CategoriaEquipamentosFilter filtro, Pageable pageable);
}
