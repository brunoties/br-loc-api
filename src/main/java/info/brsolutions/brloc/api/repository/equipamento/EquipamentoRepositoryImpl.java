package info.brsolutions.brloc.api.repository.equipamento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import info.brsolutions.brloc.api.model.Equipamento;
import info.brsolutions.brloc.api.model.Equipamento_;
import info.brsolutions.brloc.api.repository.filter.EquipamentoFilter;

public class EquipamentoRepositoryImpl implements EquipamentoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Equipamento> filtra(EquipamentoFilter filtro, Pageable pageable) {
		
		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		
		CriteriaQuery<Equipamento> criteria = builder.createQuery(Equipamento.class);

		Root<Equipamento> root = criteria.from(Equipamento.class);

		Predicate[] predicates = this.criaRestricoes(filtro, builder, root);

		criteria.where(predicates);

		TypedQuery<Equipamento> query = this.manager.createQuery(criteria);
		
		this.adicionaRestricoesPaginacao(query, pageable);

		return new PageImpl<Equipamento>(query.getResultList(), pageable, this.total(filtro));
	}

	private long total(EquipamentoFilter filtro) {
		
		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Equipamento> root = criteria.from(Equipamento.class);
		
		Predicate[] predicates = this.criaRestricoes(filtro, builder, root);
		
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		
		return this.manager.createQuery(criteria).getSingleResult();
	}

	private void adicionaRestricoesPaginacao(TypedQuery<Equipamento> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistro = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistro);
		query.setMaxResults(totalRegistrosPorPagina);
	}

	private Predicate[] criaRestricoes(EquipamentoFilter filtro, CriteriaBuilder builder, Root<Equipamento> root) {

		List<Predicate> predicates = new ArrayList<Predicate>();

		if (!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get(Equipamento_.NOME)),
					"%" + filtro.getNome().toLowerCase() + "%"));
		}

		if (!StringUtils.isEmpty(filtro.getDescricao())) {
			predicates.add(builder.like(builder.lower(root.get(Equipamento_.DESCRICAO)),
					"%" + filtro.getDescricao().toLowerCase() + "%"));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

}
