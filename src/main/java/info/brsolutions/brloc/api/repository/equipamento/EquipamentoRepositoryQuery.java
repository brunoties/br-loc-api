package info.brsolutions.brloc.api.repository.equipamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import info.brsolutions.brloc.api.model.Equipamento;
import info.brsolutions.brloc.api.repository.filter.EquipamentoFilter;

public interface EquipamentoRepositoryQuery {
	
	public Page<Equipamento> filtra(EquipamentoFilter filtro, Pageable pageable);

}
