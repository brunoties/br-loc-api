package info.brsolutions.brloc.api.repository.cliente;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import info.brsolutions.brloc.api.model.Cliente;
import info.brsolutions.brloc.api.model.Cliente_;
import info.brsolutions.brloc.api.repository.filter.ClienteFilter;

public class ClienteRepositoryImpl implements ClienteRepositoryQuery {
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Cliente> filtra(ClienteFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		
		CriteriaQuery<Cliente> criteria = builder.createQuery(Cliente.class);
		
		Root<Cliente> root = criteria.from(Cliente.class);
		
		Predicate[] predicates = this.criaRestricoes(filtro, builder, root);
		
		criteria.where(predicates);
		
		TypedQuery<Cliente> query = this.manager.createQuery(criteria);
		
		this.adicionaRestricoesPaginacao(query, pageable);
		
		return new PageImpl<Cliente>(query.getResultList(), pageable, this.total(filtro)) ;
	}

	private Predicate[] criaRestricoes(ClienteFilter filtro, CriteriaBuilder builder,
			Root<Cliente> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();
		if(!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get(Cliente_.NOME)), 
					"%" + filtro.getNome().toLowerCase() + "%"));
		}
		
		if(!StringUtils.isEmpty(filtro.getDocumentoFederal())) {
			predicates.add(builder.like(builder.lower(root.get(Cliente_.DOCUMENTO_FEDERAL)), 
					"%" + filtro.getDocumentoFederal().toLowerCase() + "%"));
		}
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionaRestricoesPaginacao(TypedQuery<Cliente> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistro = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistro);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ClienteFilter filtro) {

		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<Cliente> root = criteria.from(Cliente.class);
		
		Predicate[] predicates = this.criaRestricoes(filtro, builder, root);
		
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		
		return this.manager.createQuery(criteria).getSingleResult();
	}

}
