package info.brsolutions.brloc.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import info.brsolutions.brloc.api.model.CategoriaEquipamentos;
import info.brsolutions.brloc.api.repository.categoriaequipamentos.CategoriaEquipamentosRepositoryQuery;

public interface CategoriaEquipamentosRepository extends JpaRepository<CategoriaEquipamentos, Long>, 
CategoriaEquipamentosRepositoryQuery{
	
}
