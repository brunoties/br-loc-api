package info.brsolutions.brloc.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import info.brsolutions.brloc.api.model.Produto;
import info.brsolutions.brloc.api.repository.produto.ProdutoRepositoryQuery;

public interface ProdutoRepository extends JpaRepository<Produto, Long>, ProdutoRepositoryQuery {

}
