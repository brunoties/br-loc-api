package info.brsolutions.brloc.api.repository.cliente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import info.brsolutions.brloc.api.model.Cliente;
import info.brsolutions.brloc.api.repository.filter.ClienteFilter;

public interface ClienteRepositoryQuery {
	public Page<Cliente> filtra(ClienteFilter filtro, Pageable pageable);
}
