package info.brsolutions.brloc.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import info.brsolutions.brloc.api.model.Cliente;
import info.brsolutions.brloc.api.repository.cliente.ClienteRepositoryQuery;

public interface ClienteRepository extends JpaRepository<Cliente, Long>, ClienteRepositoryQuery{

}
