package info.brsolutions.brloc.api.repository.categoriaequipamentos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import info.brsolutions.brloc.api.model.CategoriaEquipamentos;
import info.brsolutions.brloc.api.model.CategoriaEquipamentos_;
import info.brsolutions.brloc.api.repository.filter.CategoriaEquipamentosFilter;

public class CategoriaEquipamentosRepositoryImpl implements CategoriaEquipamentosRepositoryQuery {
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<CategoriaEquipamentos> filtra(CategoriaEquipamentosFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		
		CriteriaQuery<CategoriaEquipamentos> criteria = builder.
				createQuery(CategoriaEquipamentos.class);
		
		Root<CategoriaEquipamentos> root = criteria.from(CategoriaEquipamentos.class);
		
		Predicate[] predicates = this.criaRestricoes(filtro, builder, root);
		
		criteria.where(predicates);
		
		TypedQuery<CategoriaEquipamentos> query = this.manager.createQuery(criteria);
		
		this.adicionaRestricoesPaginacao(query, pageable);
		
		return new PageImpl<CategoriaEquipamentos>(query.getResultList(), pageable, this.total(filtro));
	}

	private long total(CategoriaEquipamentosFilter filtro) {

		CriteriaBuilder builder = this.manager.getCriteriaBuilder();
		
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		
		Root<CategoriaEquipamentos> root = criteria.from(CategoriaEquipamentos.class);
		
		Predicate[] predicates = this.criaRestricoes(filtro, builder, root);
		
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		
		return this.manager.createQuery(criteria).getSingleResult();
	}

	private void adicionaRestricoesPaginacao(TypedQuery<CategoriaEquipamentos> query, Pageable pageable) {

		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistro = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistro);
		query.setMaxResults(totalRegistrosPorPagina);
		
	}

	private Predicate[] criaRestricoes(CategoriaEquipamentosFilter filtro, CriteriaBuilder builder,
			Root<CategoriaEquipamentos> root) {

		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if (!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.like(builder.lower(root.get(CategoriaEquipamentos_.NOME)), 
					"%" + filtro.getNome().toLowerCase() + "%" ));
		}
		
		if (!StringUtils.isEmpty(filtro.getDescricao())) {
			predicates.add(builder.like(builder.lower(root.get(CategoriaEquipamentos_.DESCRICAO)), 
					"%" + filtro.getDescricao().toLowerCase() + "%"));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}

}
