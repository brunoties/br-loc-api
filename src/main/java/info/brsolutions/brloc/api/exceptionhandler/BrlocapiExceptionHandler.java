package info.brsolutions.brloc.api.exceptionhandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class BrlocapiExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSource msgs;

	@ExceptionHandler({ EmptyResultDataAccessException.class })
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex,
			WebRequest request) {
		
		String msgUsuario = this.msgs.getMessage("recurso.nao_encontrato", null, 
				LocaleContextHolder.getLocale());

		String msgLog = ex.toString();

		List<Erro> erros = Arrays.asList(new Erro(msgUsuario, msgLog));
		return handleExceptionInternal(ex, erros, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	@ExceptionHandler({DataIntegrityViolationException.class})
	public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex,
			WebRequest request) {
		
		String msgUsuario = this.msgs.getMessage("recurso.associacao_obrigatoria", null, 
				LocaleContextHolder.getLocale());

		String msgLog = Optional.ofNullable(ex.getMostSpecificCause()).orElse(ex).toString();

		List<Erro> erros = Arrays.asList(new Erro(msgUsuario, msgLog));

		return handleExceptionInternal(ex, erros, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
		
	}


	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		String msgUsuario = this.msgs.getMessage("mensagem.atribudoNaoEsperado", null, 
				LocaleContextHolder.getLocale());

//		String msgLog = ex.getCause() != null ? ex.getCause().toString() : ex.toString();
		String msgLog = Optional.ofNullable(ex.getCause()).orElse(ex).toString();

		List<Erro> erros = Arrays.asList(new Erro(msgUsuario, msgLog));

		return handleExceptionInternal(ex, erros, headers, status, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		List<Erro> erros = this.criaListaErros(ex.getBindingResult());

		return handleExceptionInternal(ex, erros, headers, status, request);
	}

	private List<Erro> criaListaErros(BindingResult br) {
		List<Erro> erros = new ArrayList<BrlocapiExceptionHandler.Erro>();

		for (FieldError error : br.getFieldErrors()) {

			String msgUsuario = msgs.getMessage(error, LocaleContextHolder.getLocale());

			String msgLog = error.toString();

			erros.add(new Erro(msgUsuario, msgLog));

		}

		return erros;
	}
	
	public static class Erro {

		private String msgUsuario;
		private String msgLog;

		public Erro(String msgUsuario, String msgLog) {
			super();
			this.msgUsuario = msgUsuario;
			this.msgLog = msgLog;
		}

		public String getMsgUsuario() {
			return msgUsuario;
		}

		public String getMsgLog() {
			return msgLog;
		}

	}

}
