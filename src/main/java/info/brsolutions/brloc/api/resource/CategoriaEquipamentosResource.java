package info.brsolutions.brloc.api.resource;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import info.brsolutions.brloc.api.event.RecursoCriadoEvent;
import info.brsolutions.brloc.api.model.CategoriaEquipamentos;
import info.brsolutions.brloc.api.repository.CategoriaEquipamentosRepository;
import info.brsolutions.brloc.api.repository.filter.CategoriaEquipamentosFilter;
import info.brsolutions.brloc.api.service.CategoriaEquipamentosService;

@RestController
@RequestMapping("/categoria-equipamentos")
public class CategoriaEquipamentosResource {
	
	@Autowired
	private CategoriaEquipamentosRepository cer;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private CategoriaEquipamentosService ceService;
	
	@GetMapping
	public Page<CategoriaEquipamentos> pesquisa(CategoriaEquipamentosFilter filtro, Pageable pageable) {
		return this.cer.filtra(filtro, pageable);
	}
	
	@PostMapping
	public ResponseEntity<CategoriaEquipamentos> salva(@Valid @RequestBody CategoriaEquipamentos ce, HttpServletResponse response) {
		
		CategoriaEquipamentos ceSalva = this.cer.save(ce);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, ceSalva.getId()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(ceSalva);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<CategoriaEquipamentos> buscaPorId(@PathVariable Long id) {
		Optional<CategoriaEquipamentos> ce = this.cer.findById(id);
		return ce.isPresent() ? 
				ResponseEntity.ok(ce.get()) : ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}")
	public CategoriaEquipamentos atualiza(@PathVariable Long id, 
			@Valid @RequestBody CategoriaEquipamentos ce) {
		
		return this.ceService.atualiza(ce, id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remove(@PathVariable Long id) {
		this.cer.deleteById(id);
	}

}
