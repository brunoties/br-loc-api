package info.brsolutions.brloc.api.resource;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import info.brsolutions.brloc.api.event.RecursoCriadoEvent;
import info.brsolutions.brloc.api.model.Produto;
import info.brsolutions.brloc.api.repository.ProdutoRepository;
import info.brsolutions.brloc.api.repository.filter.ProdutoFilter;
import info.brsolutions.brloc.api.service.ProdutoService;

@RestController
@RequestMapping("/produtos")
public class ProdutoResource {
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private ProdutoService produtoService;
	
	@GetMapping
	public Page<Produto> pesquisa(ProdutoFilter filtro, Pageable pageable) {
		return this.produtoRepository.filtra(filtro, pageable);
	}
	
	@PostMapping
	public ResponseEntity<Produto> salva(@Valid @RequestBody Produto produto,
			HttpServletResponse response) {
		
		Produto produtoSalvo = this.produtoRepository.save(produto);
		
		this.publisher.publishEvent(new RecursoCriadoEvent(this, response, produtoSalvo.getId()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(produtoSalvo);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Produto> buscaPorId(@PathVariable Long id) {
		Optional<Produto> produtoRecuperado = this.produtoRepository.findById(id);
		return produtoRecuperado.isPresent() ?
				ResponseEntity.ok(produtoRecuperado.get()) :
					ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}")
	public Produto atualiza(@PathVariable Long id, @Valid @RequestBody Produto produto) {
		return this.produtoService.atualiza(produto, id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remove(@PathVariable Long id) {
		this.produtoRepository.deleteById(id);
	}

}
