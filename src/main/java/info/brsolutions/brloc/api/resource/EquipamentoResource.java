package info.brsolutions.brloc.api.resource;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import info.brsolutions.brloc.api.event.RecursoCriadoEvent;
import info.brsolutions.brloc.api.model.Equipamento;
import info.brsolutions.brloc.api.repository.EquipamentoRepository;
import info.brsolutions.brloc.api.repository.filter.EquipamentoFilter;
import info.brsolutions.brloc.api.service.EquipamentoService;

@RestController
@RequestMapping("/equipamentos")
public class EquipamentoResource {

	@Autowired
	private ApplicationEventPublisher publisher;

	@Autowired
	private EquipamentoRepository equipamentoRepository;

	@Autowired
	private EquipamentoService equipamentoService;

	@GetMapping
	public Page<Equipamento> pesquisa(EquipamentoFilter filtro, Pageable pageable) {
		return this.equipamentoRepository.filtra(filtro, pageable);
	}

	@PostMapping
	public ResponseEntity<Equipamento> salva(@Valid @RequestBody Equipamento equipamento,
			HttpServletResponse response) {

		Equipamento equipamentoSalvo = this.equipamentoRepository.save(equipamento);

		this.publisher.publishEvent(new RecursoCriadoEvent(this, response, equipamentoSalvo.getId()));

		return ResponseEntity.status(HttpStatus.CREATED).body(equipamentoSalvo);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Equipamento> buscaPorId(@PathVariable Long id) {
		Optional<Equipamento> equipamentoRecuperado = this.equipamentoRepository.findById(id);
		return equipamentoRecuperado.isPresent() ?
				ResponseEntity.ok(equipamentoRecuperado.get()) :
					ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}")
	public Equipamento atualiza(@PathVariable Long id, @Valid @RequestBody Equipamento equipamento) {
		return this.equipamentoService.atualiza(equipamento, id);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remove(@PathVariable Long id) {
		this.equipamentoRepository.deleteById(id);
	}
}
