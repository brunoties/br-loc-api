package info.brsolutions.brloc.api.resource;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import info.brsolutions.brloc.api.event.RecursoCriadoEvent;
import info.brsolutions.brloc.api.model.Cliente;
import info.brsolutions.brloc.api.repository.ClienteRepository;
import info.brsolutions.brloc.api.repository.filter.ClienteFilter;
import info.brsolutions.brloc.api.service.ClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteResource {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping
	public Page<Cliente> pesquisa(ClienteFilter filtro, Pageable pageable) {
		return this.clienteRepository.filtra(filtro, pageable);
	}
	
	@PostMapping
	public ResponseEntity<Cliente> salva(@Valid @RequestBody Cliente cliente, HttpServletResponse response) {
		
		Cliente clienteSalvo = this.clienteRepository.save(cliente);
		
		this.publisher.publishEvent(new RecursoCriadoEvent(this, response, clienteSalvo.getId()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteSalvo);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Cliente> buscaPorId(@PathVariable Long id) {
		Optional<Cliente> clienteRecuperado = this.clienteRepository.findById(id);
		return clienteRecuperado.isPresent() ?
				ResponseEntity.ok(clienteRecuperado.get()) :
					ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}")
	public Cliente atualiza(@PathVariable Long id, 
			@Valid @RequestBody Cliente cliente) {
		
		return this.clienteService.atualiza(cliente, id);
	}
	
	@PutMapping("/{id}/ativo")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void atualizaAtivo(@PathVariable Long id, @RequestBody Boolean ativo) {
		this.clienteService.atualizaAtivo(id, ativo);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remove(@PathVariable Long id) {
		this.clienteRepository.deleteById(id);
	}

}
