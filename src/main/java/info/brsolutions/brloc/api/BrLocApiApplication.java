package info.brsolutions.brloc.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BrLocApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrLocApiApplication.class, args);
	}

}
