package info.brsolutions.brloc.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cliente.class)
public abstract class Cliente_ {

	public static volatile SingularAttribute<Cliente, Boolean> ativo;
	public static volatile SingularAttribute<Cliente, TipoCliente> tipo;
	public static volatile SingularAttribute<Cliente, Endereco> endereco;
	public static volatile SingularAttribute<Cliente, String> nome;
	public static volatile SingularAttribute<Cliente, Long> id;
	public static volatile SingularAttribute<Cliente, String> documentoFederal;

	public static final String ATIVO = "ativo";
	public static final String TIPO = "tipo";
	public static final String ENDERECO = "endereco";
	public static final String NOME = "nome";
	public static final String ID = "id";
	public static final String DOCUMENTO_FEDERAL = "documentoFederal";

}

