package info.brsolutions.brloc.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CategoriaEquipamentos.class)
public abstract class CategoriaEquipamentos_ {

	public static volatile SingularAttribute<CategoriaEquipamentos, String> nome;
	public static volatile SingularAttribute<CategoriaEquipamentos, Long> id;
	public static volatile ListAttribute<CategoriaEquipamentos, Equipamento> equipamentos;
	public static volatile SingularAttribute<CategoriaEquipamentos, String> descricao;

	public static final String NOME = "nome";
	public static final String ID = "id";
	public static final String EQUIPAMENTOS = "equipamentos";
	public static final String DESCRICAO = "descricao";

}

