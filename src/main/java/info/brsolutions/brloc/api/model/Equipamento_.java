package info.brsolutions.brloc.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Equipamento.class)
public abstract class Equipamento_ {

	public static volatile ListAttribute<Equipamento, CategoriaEquipamentos> categorias;
	public static volatile SingularAttribute<Equipamento, String> nome;
	public static volatile SingularAttribute<Equipamento, Long> id;
	public static volatile SingularAttribute<Equipamento, String> descricao;

	public static final String CATEGORIAS = "categorias";
	public static final String NOME = "nome";
	public static final String ID = "id";
	public static final String DESCRICAO = "descricao";

}

