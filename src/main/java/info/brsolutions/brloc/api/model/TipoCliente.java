package info.brsolutions.brloc.api.model;

public enum TipoCliente {
	PESSOA_JURIDICA,
	PESSOA_FISICA
}
