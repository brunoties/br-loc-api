package info.brsolutions.brloc.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import info.brsolutions.brloc.api.model.Equipamento;
import info.brsolutions.brloc.api.repository.EquipamentoRepository;

@Service
public class EquipamentoService {
	
	@Autowired
	private EquipamentoRepository equipamentoRepository;

	public Equipamento atualiza(Equipamento equipamento, Long id) {
		Equipamento equipamentoRecuperado = this.buscaPorId(id);
		BeanUtils.copyProperties(equipamento, equipamentoRecuperado, "id");
		return this.equipamentoRepository.save(equipamentoRecuperado);
	}

	private Equipamento buscaPorId(Long id) {
		Equipamento equipamentoRecuperado = this.equipamentoRepository.findById(id)
				.orElseThrow(() -> new EmptyResultDataAccessException(1));
		return equipamentoRecuperado;
	}

}
