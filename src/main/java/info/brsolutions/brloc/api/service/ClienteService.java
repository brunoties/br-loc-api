package info.brsolutions.brloc.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import info.brsolutions.brloc.api.model.Cliente;
import info.brsolutions.brloc.api.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	public Cliente atualiza(Cliente cliente, Long id) {
		
		Cliente clienteSalvo = this.buscaPorId(id);
		
		BeanUtils.copyProperties(cliente, clienteSalvo, "id");
		
		return this.clienteRepository.save(clienteSalvo);

	}

	public void atualizaAtivo(Long id, Boolean ativo) {
		
		Cliente clienteSalvo = this.buscaPorId(id);
		
		clienteSalvo.setAtivo(ativo);
		
		this.clienteRepository.save(clienteSalvo);

	}
	
	private Cliente buscaPorId(Long id) {
		Cliente clienteSalvo = this.clienteRepository.findById(id)
				.orElseThrow(() -> new EmptyResultDataAccessException(1));
		return clienteSalvo;
	}

}
