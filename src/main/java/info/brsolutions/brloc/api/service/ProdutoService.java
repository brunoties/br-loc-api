package info.brsolutions.brloc.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import info.brsolutions.brloc.api.model.Produto;
import info.brsolutions.brloc.api.repository.ProdutoRepository;

@Service
public class ProdutoService {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Produto atualiza(Produto produto, Long id) {
		Produto produtoRecuperado = this.buscaPorId(id);
		BeanUtils.copyProperties(produto, produtoRecuperado, "id");
		return this.produtoRepository.save(produtoRecuperado);
	}

	private Produto buscaPorId(Long id) {
		Produto produtoRecuperado = this.produtoRepository.findById(id)
				.orElseThrow(() -> new EmptyResultDataAccessException(1));
		return produtoRecuperado;
	}

}
