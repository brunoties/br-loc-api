package info.brsolutions.brloc.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import info.brsolutions.brloc.api.model.CategoriaEquipamentos;
import info.brsolutions.brloc.api.repository.CategoriaEquipamentosRepository;

@Service
public class CategoriaEquipamentosService {
	
	@Autowired
	private CategoriaEquipamentosRepository ceRepository;
	
	public CategoriaEquipamentos atualiza(CategoriaEquipamentos ce, Long id) {
		
		CategoriaEquipamentos ceSalvo = this.buscaPorId(id);
		
		BeanUtils.copyProperties(ce, ceSalvo, "id");
		
		return this.ceRepository.save(ceSalvo);
 
	}

	private CategoriaEquipamentos buscaPorId(Long id) {
		return this.ceRepository.findById(id)
				.orElseThrow(() -> new EmptyResultDataAccessException(1));
	}
	
	

}
