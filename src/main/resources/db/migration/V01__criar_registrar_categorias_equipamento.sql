create table cliente (
	id_cliente bigint(20) primary key auto_increment,
    ativo boolean not null,
    tipo VARCHAR(30) NOT NULL,
    nome varchar(100) not null,
    documento_federal varchar(20) not null,
    logradouro varchar(100),
    numero varchar(100),
    complemento varchar(100),
    bairro varchar(100),
    codigo_postal varchar(100),
    cidade varchar(100),
    estado varchar(100),
    pais varchar(100)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(true, 'Manuel', '987', 'PESSOA_FISICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(true, 'Huguinho', '111', 'PESSOA_FISICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(false, 'Juquinha', '321', 'PESSOA_FISICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(true, 'Pedro', '987', 'PESSOA_FISICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(true, 'Dudu', '111', 'PESSOA_FISICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(false, 'João', '321', 'PESSOA_FISICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(true, 'BR Solutions', '987', 'PESSOA_JURIDICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(true, 'Fugro', '111', 'PESSOA_JURIDICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(false, 'WTF', '321', 'PESSOA_JURIDICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(false, 'Queiroz', '321', 'PESSOA_JURIDICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(true, 'Ocean', '987', 'PESSOA_JURIDICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(false, 'FMC', '321', 'PESSOA_JURIDICA');

INSERT INTO cliente(ativo, nome, documento_federal, tipo) 
	VALUES(true, 'SLB', '987', 'PESSOA_JURIDICA');


CREATE TABLE equipamento (
	id_equipamento BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	descricao VARCHAR(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO equipamento(nome, descricao) 
	VALUES('Empilhadeira', '1.0 TON');

INSERT INTO equipamento(nome, descricao) 
	VALUES('Empilhadeira', '2.0 TON');

INSERT INTO equipamento(nome, descricao) 
	VALUES('Empilhadeira', '3.0 TON');

INSERT INTO equipamento(nome, descricao) 
	VALUES('Empilhadeira', '4.0 TON');

INSERT INTO equipamento(nome, descricao) 
	VALUES('Empilhadeira', '5.0 TON');

INSERT INTO equipamento(nome, descricao) 
	VALUES('Empilhadeira', '6.0 TON');

INSERT INTO equipamento(nome, descricao) 
	VALUES('Empilhadeira', '7.0 TON');

INSERT INTO equipamento(nome, descricao) 
	VALUES('Empilhadeira', '8.0 TON');

INSERT INTO equipamento(nome, descricao) 
	VALUES('Empilhadeira', '9.0 TON');

INSERT INTO equipamento(nome, descricao) 
	VALUES('Empilhadeira', '10.0 TON');

CREATE TABLE categoria_equipamentos (
	id_categoria_equipamentos BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	descricao VARCHAR(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO categoria_equipamentos(nome, descricao) 
	VALUES('Dimensões da Maquina AxLxC:', '2180MMx1180MMx1900MM');

INSERT INTO categoria_equipamentos(nome, descricao) 
	VALUES('Dimensões Garfo AxLxC:', '100MMx1060MMx2100MM');

INSERT INTO categoria_equipamentos(nome, descricao) 
	VALUES('Carrinho AxLxC:', '400MMx1020MM');


CREATE TABLE equipamentos_categorias (
	id_equipamento bigint(20) not null, 
	id_categoria_equipamentos BIGINT(20) not null,

	foreign key (id_equipamento)
		references equipamento (id_equipamento),

	foreign key (id_categoria_equipamentos)
		references categoria_equipamentos (id_categoria_equipamentos),

	primary key (id_equipamento, id_categoria_equipamentos)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into equipamentos_categorias (id_equipamento, 
	id_categoria_equipamentos)
	values(1,1);

insert into equipamentos_categorias (id_equipamento, 
	id_categoria_equipamentos)
	values(2,2);

insert into equipamentos_categorias (id_equipamento, 
	id_categoria_equipamentos)
	values(3,3);