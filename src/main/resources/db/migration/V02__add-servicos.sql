create table produto (
	id_produto bigint(20) primary key auto_increment,
    ativo boolean not null,
    nome varchar(100) not null,
    descricao VARCHAR(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO produto(ativo, nome, descricao) 
	VALUES(true, 'Empilhadeira', '1.0 TON');

INSERT INTO produto(ativo, nome, descricao) 
	VALUES(true,'Operador', 'Empilhadeira');

INSERT INTO produto(ativo, nome, descricao) 
	VALUES(false,'Operador', 'Munk');

